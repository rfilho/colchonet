FactoryGirl.define do
  factory :booking do
     start_at Date.today
     end_at 2.days.since(Date.today)
     
     after(:build) do |booking|
        booking.room = FactoryGirl.create(:room) if booking.room.nil?
     end
  end
end
