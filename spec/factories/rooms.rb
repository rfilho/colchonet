FactoryGirl.define do
  factory :room do
     number 1
     description "Quarto com ar-condicionado, frigobar e tv"
  end
end
