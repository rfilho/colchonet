# encoding: utf-8
require 'spec_helper'

feature 'create a new booking' do
  scenario 'with valid dates for coming and exit' do
     room = Room.create :number => 1, :description => 'Quarto com frigobar, arcondicionado e tv'
     visit "/rooms/#{room.id}"
     click_link 'Fazer reserva'
     select_date 1.month.since(Date.today), :from => 'Data de entrada'
     select_date 2.month.since(Date.tomorrow), :from => 'Data de saída'
     click_button 'Criar Reserva'
     expect(page).to have_content('Reserva efetuada com sucesso!')
  end
end
