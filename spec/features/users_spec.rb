require 'spec_helper'

feature 'Sign up new user' do

  before :each do
    visit sign_up_path
  end

  scenario "is sucessful with valid email, password, password confirmation and full name" do
      expect{
        fill_in 'E-mail', with: 'renatosousafilho@gmail.com'
        fill_in 'Senha', with: '1234'
        fill_in 'Confirme sua senha', with: '1234'
        fill_in 'Nome completo', with: 'Renato Filho'
        click_button 'Criar Usuário'
      }.to change(User, :count).by(1)
      expect(page).to have_content 'Usuário cadastrado com sucesso!'
  end

  scenario "is unsucessful without email" do
     expect{
       fill_in 'E-mail', with: nil
       click_button 'Criar Usuário'
     }.to_not change(User, :count).by(1)
     expect(page).to have_content("E-mail não pode ficar em branco")
  end
  
  scenario "is unsucessful without password confirmation" do
  end
  
  scenario "is unsucessful with password and password confirmation that doesn't matches" do
    expect{
       fill_in 'Senha', with: '1234'
       fill_in 'Confirme sua senha', with: '123'
       click_button 'Criar Usuário'
    }.to_not change(User, :count).by(1)
    expect(page).to have_content("sua senha não está de acordo com a confirmação")
  end
end

feature 'Edit user data' do
   before :each do
      @user = User.create :email => 'renatosousafilho@gmail.com', :password => '1234', :password_confirmation => '1234', :full_name => 'Renato Filho'
   end

   scenario 'with valid attributes' do
      visit "/users/#{@user.id}/edit"
      expect{
        fill_in 'Senha', :with => '12345'
        fill_in 'Confirme sua senha', :with => '12345'
        click_button 'Atualizar Usuário'
      }.to change(@user, :password_digest)
      expect(page).to have_content('Usuário editado com sucesso!')
   end

end
