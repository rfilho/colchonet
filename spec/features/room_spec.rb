# encoding: utf-8
require 'spec_helper'


feature 'create a new room' do
  scenario 'with valid data' do
     visit new_room_path
     fill_in I18n.t('activerecord.attributes.room.number'), :with => 1
     fill_in I18n.t('activerecord.attributes.room.description'), :with => "Quarto com ar condicionado, refrigerador e frigobar"
     click_button 'Criar Quarto'
     expect(page).to have_content "Quarto criado com sucesso!"
  end
end
