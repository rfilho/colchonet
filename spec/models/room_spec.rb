require 'spec_helper'

describe Room do
  it "is valid with number and description" do
     expect(build(:room)).to be_valid
  end

  it 'is not valid without number' do
     expect(build(:room, number: nil)).to_not be_valid
  end

  it 'is not valid without description' do
     expect(build(:room, description: nil)).to_not be_valid
  end

  it 'is invalid with duplicata number' do
     create(:room)
     expect(build(:room)).to have(1).errors_on(:number)
  end
end
