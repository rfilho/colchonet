require 'spec_helper'

describe User do
  it 'is valid with username, email and password' do
    user = User.create :full_name => 'renato', :email => 'renatosousafilho@gmail.com', :password => 'senhacorreta', :password_confirmation => 'senhacorreta'
    expect(user).to be_valid
  end

  it 'is invalid without full name' do
    user = User.new :full_name => nil
    expect(user).to_not be_valid
  end
  
  it 'is invalid without email' do
    user = User.new :email => nil
    expect(user).to_not be_valid
  end

  it 'is invalid without password' do
    user = User.new :password => nil
    expect(user).to_not be_valid
  end

  it 'has a unique record for same email' do
    user = User.create :email=> 'renatosousafilho@gmail.com', :password => 'senhacorreta', :password_confirmation => 'senhacorreta', :full_name => 'renato sousa filho'
    expect(user).to be_valid
    expect(User.new(:email=>'renatosousafilho@gmail.com')).to_not be_valid
  end
 
  it 'is invalid with a password does not matches password confirmation' do
    user = User.new :email=> 'renatosousafilho@gmail.com', :password => 'senhacerta', :password_confirmation => 'senhaerrada'
    expect(user).to_not be_valid
  end

  it 'is invalid with a password does not matches password confirmation' do
    user = User.new :email=> 'renatosousafilho@gmail.com', :password => 'senhacerta', :password_confirmation => 'senhacerta'
    expect(user).to_not be_valid
  end
end
