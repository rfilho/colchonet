require 'spec_helper'

describe Booking do
  it 'is valid with start_at and end_at dates and room' do
     expect(build(:booking)).to be_valid
  end

  it 'is invalid without start_at date' do
     expect(build(:booking, start_at: nil)).to have(1).errors_on(:start_at)
  end

  it 'is invalid without end_at date' do
     expect(build(:booking, end_at: nil)).to have(1).errors_on(:end_at)
  end

  it 'is invalid without room id' do
     expect(create(:booking, room: nil)).to_not be_valid
  end

  context 'to same room 'do
     before :all do
       @booking = create(:booking)
     end

     it 'cannot start at between reserved date' do
        booking = build(:booking, :start_at => 1.day.since(@booking.start_at), room: @booking.room)
        expect(booking).to have(1).errors_on(:start_at) 
     end

     it 'cannot end at between reserved date' do
        booking = build(:booking, :end_at => 1.day.since(@booking.start_at), room: @booking.room)
        expect(booking).to have(1).errors_on(:end_at) 
     end
  end
     
  it 'cannot start at or end_at in the past' do
     expect(build(:booking, start_at: 5.days.ago)).to_not be_valid
  end

  it 'cannot end at on past' do
     expect(build(:booking, end_at: 5.days.ago)).to_not be_valid
  end

  it 'cannot start at after end at' do
     expect(build(:booking, end_at: Date.today, start_at: Date.tomorrow)).to_not be_valid
  end

end
