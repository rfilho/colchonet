Colchonet::Application.routes.draw do
  get "bookings/index"
  get '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  root :to => 'bookings#index'
  get 'sign_up' => 'users#new', :as => 'sign_up'
  resources :users
  resources :rooms do
     resources :bookings, only: [:new, :create, :update], module: :rooms
  end
  resources :bookings, only: [:index, :show]
end
