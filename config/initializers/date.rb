Date.class_eval do
   def after?(time)
      time > self if time.is_a?(Date)
   end

   def before?(time)
      time < self if time.is_a?(Date)
   end
   
   def is_past?
      self < Date.today
   end
end
