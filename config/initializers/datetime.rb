ActiveSupport::TimeWithZone.class_eval do
   def after?(time)
      self > time
   end

   def before?(time)
      self < time
   end
end
