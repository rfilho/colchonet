Time.class_eval do
   def after?(time)
      time > self if time.is_a?(Time)
   end

   def before?(time)
      time < self if time.is_a?(Time)
   end
end
