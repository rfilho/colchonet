Fixnum.class_eval do
  def after(date)
     date + self if date.is_a?(Date)
  end

  def before(date)
     date - self if date.is_a?(Date)
  end
end
