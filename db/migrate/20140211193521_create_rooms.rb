class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.integer :number
      t.text :description
      t.string :title

      t.timestamps
    end
  end
end
