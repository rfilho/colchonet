class Rooms::BookingsController < ApplicationController
   def new
       @booking = room.bookings.build
   end

   def create
      @booking = room.bookings.build booking_params
      if @booking.save
          redirect_to bookings_path, :notice => 'Reserva efetuada com sucesso!'
      else
          render :new
      end
   end

   private
   def room
      @room ||= Room.find(params[:room_id])
   end

   def booking_params
       params.require(:booking).permit(:start_at, :end_at	)
   end
   

end
