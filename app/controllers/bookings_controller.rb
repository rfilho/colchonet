class BookingsController < ApplicationController
  def index
    @month = (params[:month] || Time.zone.now.month).to_i
    @year = (params[:year] || Time.zone.now.year).to_i

    @shown_month = Date.civil(@year, @month)

    @event_strips = Booking.event_strips_for_month(@shown_month)
  end
  
  def new
     @booking = Booking.new
  end

  def create
     @booking = Booking.new(booking_params)
     if @booking.save
        redirect_to @booking, :notice => 'Reserva efetuada com sucesso!'
     else
        render :new
     end
  end

  def show
     @booking = Booking.find(params[:id])
  end

  private

  def booking_params
     params.require(:booking).permit(:start_at, :end_at)
  end 
end
