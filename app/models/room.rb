class Room < ActiveRecord::Base
   validates_presence_of :number, :description
   validates_uniqueness_of :number

   has_many :bookings do
     def persisted
        collect { |booking| booking if booking.persisted? }
     end     
   end

   def reserved_in?(date)
      return bookings.exists?(['start_at<=? and end_at>=?', date, date])
   end

end
