class Booking < ActiveRecord::Base
  belongs_to :room 
  validates_presence_of :start_at, :end_at, :room_id

  validate :cannot_start_or_end_at_reserved_date, :cannot_end_at_be_less_than_or_equal_to_start_at, :if => lambda{ |object| object.start_at.present? and object.end_at.present? }
  validate :cannot_start_at_past, :if => lambda{ |object| object.start_at.present? }
  validate :cannot_end_at_past, :if => lambda{ |object| object.end_at.present?}



  def cannot_start_or_end_at_reserved_date
    errors.add(:start_at, :exclusion) if room.reserved_in?(start_at.to_date)
    errors.add(:end_at, :exclusion) if room.reserved_in?(end_at.to_date) 
  end

  def cannot_end_at_be_less_than_or_equal_to_start_at
    errors.add(:end_at, :invalid) unless end_at.after? start_at
  end
  
  def cannot_start_at_past
    errors.add(:start_at, :exclusion) if self.start_at.to_date.is_past?
  end

  def cannot_end_at_past
    errors.add(:end_at, :exclusion) if self.end_at.to_date.is_past?
  end

  has_event_calendar

end
