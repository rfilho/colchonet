class Fixnum
  def after(date)
    date + self
  end
  
  def before(date)
    date - self
  end
end
